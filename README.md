# terraform-skeleton
When I first started with Terraform a few years ago, I wanted to have my repositories structured in a logical fashion, but nothing I found seemed to fit what I was looking for.  I searched the internet to find out what HashiCorp recommended and what other people are doing, but nothin I saw really felt right to me.  In examining what other people were doing, however, I was able to take the best parts from various layouts and meld it into a structure that worked for me and I have been using it ever since.

I expect that many will look at my layout and feel the same way I did about many of the ones I looked at.  Hopefully, you will be able to take a way at least a nugget or two to help you with your structure.  You can check out the GitHub repo here.

## Directory Layout
The directory layout is pretty straight forward.  It contains a directory for all the project variables and Terraform files as well as a Makefile and Jenkinsfile for automation.  The file tree of the directory looks like this:

    .
    ├── backend.tf
    ├── provider.tf
    ├── main.tf
    ├── variables.tf
    ├── outputs.tf
    ├── statefile.tf 
    ├── README.md
    └── LICENSE

### Terraform Files
Rather than cramming everything into a single file, I tend to use more files rather than less for readability.  To that end I generally have 5 .tf files that I use when working with Terraform.

#### backend.tf
The backend.tf file contains information about which backend to use (S3 in my case).

#### provider.tf
The provider.tf file contains which provider to use.  My directory defaults to the AWS provider, but I have used Azure and GCP as well.

#### main.tf
This is where I define which modules I want to use.  Now that Terraform has a module registry, I try to use that as much as possible, but occasionally I will write my own.

#### variables.tf
The variables.tf file is used to initialize all the variables that I want to pass in via my projects file.

#### outputs.tf
The outputs.tf file is for storing any outputs that you may want to make available to other Terraform projects at a later time.

